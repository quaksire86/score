package com.quaksire.checkyourscore.ui

import com.quaksire.checkyourscore.ScoreTest
import com.quaksire.checkyourscore.use.ScoreModel
import com.quaksire.checkyourscore.use.ScoreUseCase
import com.quaksire.checkyourscore.use.State
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.lang.RuntimeException

class ScoreViewModelTest: ScoreTest() {

    private lateinit var scoreViewModel: ScoreViewModel

    @MockK
    private lateinit var mockScoreUseCase: ScoreUseCase

    private val fixtureScoreModel = fixture<ScoreModel>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        scoreViewModel = ScoreViewModel(mockScoreUseCase)
    }

    @Test
    fun `when useCase returns a valid response, flow return loading to success transition`() = runBlocking {
        coEvery {
            mockScoreUseCase.getUserScore()
        } returns fixtureScoreModel

        val stateValues = scoreViewModel.scoreFlow.toList()

        assertEquals(2, stateValues.size)
        assertTrue(stateValues[0] is State.Loading<ScoreModel>)
        assertTrue(stateValues[1] is State.Success<ScoreModel>)
        assertEquals(fixtureScoreModel, (stateValues[1] as State.Success<ScoreModel>).value)
    }

    @Test
    fun `when useCase throws exception, flow return loading to error transition`() = runBlocking {
        coEvery {
            mockScoreUseCase.getUserScore()
        } throws RuntimeException("Test")

        val stateValues = scoreViewModel.scoreFlow.toList()

        assertEquals(2, stateValues.size)
        assertTrue(stateValues[0] is State.Loading<ScoreModel>)
        assertTrue(stateValues[1] is State.Error<ScoreModel>)
    }
}