package com.quaksire.checkyourscore.use

import com.quaksire.checkyourscore.ScoreTest
import com.quaksire.checkyourscore.domain.Score
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class UseCaseScoreMapperTest: ScoreTest() {

    private lateinit var mapper: UseCaseScoreMapper

    private val fixtureScore = fixture<Score>()

    @Before
    fun setUp() {
        mapper = UseCaseScoreMapper()
    }

    @Test
    fun `mapper can map correct value`() {
        val result = mapper.map(fixtureScore)

        assertEquals(fixtureScore.creditReportInfo.score, result.score)
        assertEquals(fixtureScore.creditReportInfo.maxScore, result.maxScore)
        assertEquals(fixtureScore.creditReportInfo.minScore, result.minScore)
    }
}