package com.quaksire.checkyourscore.use

import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.ScoreTest
import com.quaksire.checkyourscore.domain.Score
import com.quaksire.checkyourscore.repository.ScoreRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.lang.ClassCastException
import java.lang.RuntimeException

class ScoreUseCaseImplTest : ScoreTest() {

    private lateinit var scoreUseCase: ScoreUseCase

    @MockK
    private lateinit var scoreRepository: ScoreRepository

    @MockK
    private lateinit var scoreUseCaseMapper: ScoreMapper<Score, ScoreModel>

    private val fixtureScore = fixture<Score>()
    private val fixtureScoreModel = fixture<ScoreModel>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        this.scoreUseCase = ScoreUseCaseImpl(
            scoreRepository,
            scoreUseCaseMapper
        )
    }

    @Test
    fun `when repository returns valid response, useCase return a valid response`() {
        coEvery {
            scoreRepository.getScore()
        } returns fixtureScore
        every {
            scoreUseCaseMapper.map(fixtureScore)
        } returns fixtureScoreModel

        val result = runBlocking {
            scoreUseCase.getUserScore()
        }

        coVerify {
            scoreUseCaseMapper.map(fixtureScore)
        }
        assertEquals(fixtureScoreModel, result)
    }

    @Test(expected = ScoreUseCaseException::class)
    fun `when repository throws exception, throw ScoreUseCaseException`() {
        coEvery {
            scoreRepository.getScore()
        } throws RuntimeException()

        runBlocking {
            scoreUseCase.getUserScore()
        }
    }

    @Test(expected = ScoreUseCaseException::class)
    fun `when repository returns valid response but mapper cannot parse, throw ScoreUseCaseException`() {
        coEvery {
            scoreRepository.getScore()
        } returns fixtureScore
        every {
            scoreUseCaseMapper.map(fixtureScore)
        } throws ClassCastException()

        runBlocking {
            scoreUseCase.getUserScore()
        }
    }
}