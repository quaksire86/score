package com.quaksire.checkyourscore.repository

import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.ScoreTest
import com.quaksire.checkyourscore.api.AwsApi
import com.quaksire.checkyourscore.api.dto.ScoreDto
import com.quaksire.checkyourscore.domain.Score
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class ScoreRepositoryImplTest : ScoreTest() {

    private lateinit var repository: ScoreRepository

    @MockK
    private lateinit var mockAwsApi: AwsApi

    @MockK
    private lateinit var mockScoreMapper: ScoreMapper<ScoreDto, Score>

    private val fixtureScoreDto = fixture<ScoreDto>()
    private val fixtureScore = fixture<Score>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        this.repository = ScoreRepositoryImpl(mockAwsApi, mockScoreMapper)
    }

    @Test
    fun `given getScore API returns a valid result, then repository should return valid Score`() {
        // Given
        coEvery { mockAwsApi.getScore() } returns fixtureScoreDto
        every { mockScoreMapper.map(fixtureScoreDto) } returns fixtureScore

        // When
        val result = runBlocking {
            repository.getScore()
        }

        // Then
        verify { mockScoreMapper.map(fixtureScoreDto) }
        assertEquals(fixtureScore, result)
    }

}