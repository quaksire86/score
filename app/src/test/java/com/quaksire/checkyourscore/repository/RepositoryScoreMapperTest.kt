package com.quaksire.checkyourscore.repository

import com.quaksire.checkyourscore.ScoreTest
import com.quaksire.checkyourscore.api.dto.ScoreDto
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class RepositoryScoreMapperTest: ScoreTest() {

    private lateinit var mapper: RepositoryScoreMapper

    private val fixtureScoreDto = fixture<ScoreDto>()

    @Before
    fun setUp() {
        mapper = RepositoryScoreMapper()
    }

    @Test
    fun `mapper should map the correct dto`() {
        val result = mapper.map(fixtureScoreDto)
        assertEquals(fixtureScoreDto.creditReportInfo?.score, result.creditReportInfo.score)
        assertEquals(fixtureScoreDto.creditReportInfo?.maxScore, result.creditReportInfo.maxScore)
        assertEquals(fixtureScoreDto.creditReportInfo?.minScore, result.creditReportInfo.minScore)
    }
}