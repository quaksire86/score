package com.quaksire.checkyourscore

interface ScoreMapper<T, V> {
    fun map(t: T): V
}