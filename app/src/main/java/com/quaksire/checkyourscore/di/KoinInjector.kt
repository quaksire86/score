package com.quaksire.checkyourscore.di

import com.google.gson.GsonBuilder
import com.quaksire.checkyourscore.BuildConfig
import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.api.AwsApi
import com.quaksire.checkyourscore.api.dto.ScoreDto
import com.quaksire.checkyourscore.domain.Score
import com.quaksire.checkyourscore.repository.RepositoryScoreMapper
import com.quaksire.checkyourscore.repository.ScoreRepository
import com.quaksire.checkyourscore.repository.ScoreRepositoryImpl
import com.quaksire.checkyourscore.ui.ScoreViewModel
import com.quaksire.checkyourscore.use.ScoreModel
import com.quaksire.checkyourscore.use.ScoreUseCase
import com.quaksire.checkyourscore.use.ScoreUseCaseImpl
import com.quaksire.checkyourscore.use.UseCaseScoreMapper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun provideApiModule() = module {
    single<Retrofit> {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .baseUrl(BuildConfig.API_URL)
            .build()
    }
    single<AwsApi> { get<Retrofit>().create(AwsApi::class.java)}
}

fun provideRepositoryModule() = module {
    single<ScoreMapper<ScoreDto, Score>>(named("repositoryScoreMapper")) { RepositoryScoreMapper() }
    factory<ScoreRepository> { ScoreRepositoryImpl(get(), get(qualifier = named("repositoryScoreMapper"))) }
}

fun provideUseCaseModule() = module {
    single<ScoreMapper<Score, ScoreModel>>(named("useCaseMapper")) { UseCaseScoreMapper() }
    factory<ScoreUseCase> { ScoreUseCaseImpl(get(), get(qualifier = named("useCaseMapper"))) }
}

fun provideViewModelModule() = module {
    viewModel {
        ScoreViewModel(get())
    }
}