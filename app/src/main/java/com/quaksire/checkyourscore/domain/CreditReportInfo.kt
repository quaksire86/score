package com.quaksire.checkyourscore.domain

data class CreditReportInfo(
    val score: Int,
    val maxScore: Int,
    val minScore: Int
)