package com.quaksire.checkyourscore.domain

data class Score(
    val creditReportInfo: CreditReportInfo
)