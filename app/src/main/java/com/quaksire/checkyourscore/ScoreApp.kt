package com.quaksire.checkyourscore

import android.app.Application
import com.quaksire.checkyourscore.di.provideApiModule
import com.quaksire.checkyourscore.di.provideRepositoryModule
import com.quaksire.checkyourscore.di.provideUseCaseModule
import com.quaksire.checkyourscore.di.provideViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ScoreApp: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@ScoreApp)

            modules(
                provideApiModule(),
                provideRepositoryModule(),
                provideUseCaseModule(),
                provideViewModelModule()
            )
        }
    }
}