package com.quaksire.checkyourscore.repository

import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.api.AwsApi
import com.quaksire.checkyourscore.api.dto.ScoreDto
import com.quaksire.checkyourscore.domain.Score

interface ScoreRepository {
    suspend fun getScore(): Score
}

class ScoreRepositoryImpl(
    private val awsApi: AwsApi,
    private val scoreMapper: ScoreMapper<ScoreDto, Score>
): ScoreRepository {

    override suspend fun getScore(): Score {
        val scoreDto = awsApi.getScore()
        return scoreMapper.map(scoreDto)
    }
}