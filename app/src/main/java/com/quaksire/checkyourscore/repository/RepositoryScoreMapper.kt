package com.quaksire.checkyourscore.repository

import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.api.dto.CreditReportInfoDto
import com.quaksire.checkyourscore.api.dto.ScoreDto
import com.quaksire.checkyourscore.domain.CreditReportInfo
import com.quaksire.checkyourscore.domain.Score

class RepositoryScoreMapper: ScoreMapper<ScoreDto, Score> {
    override fun map(t: ScoreDto): Score {
        if (t.creditReportInfo == null) {
            throw MissingInformationException("creditReportInfo is null")
        } else {
            return Score(
                creditReportInfo = mapCreditReportDto(t.creditReportInfo)
            )
        }
    }

    private fun mapCreditReportDto(dto: CreditReportInfoDto): CreditReportInfo {
        return CreditReportInfo(
            score = dto.score,
            maxScore = dto.maxScore,
            minScore = dto.minScore
        )
    }
}