package com.quaksire.checkyourscore.repository

import java.lang.Exception

class MissingInformationException(message: String): Exception(message) {
}