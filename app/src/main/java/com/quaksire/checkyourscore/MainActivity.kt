package com.quaksire.checkyourscore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.quaksire.checkyourscore.ui.ScoreFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ScoreFragment())
                .commitNow()
        }
    }
}