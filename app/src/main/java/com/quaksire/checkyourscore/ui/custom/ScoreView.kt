package com.quaksire.checkyourscore.ui.custom

import android.content.Context
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import app.futured.donut.DonutProgressView
import app.futured.donut.DonutSection
import com.quaksire.checkyourscore.R
import com.quaksire.checkyourscore.use.ScoreModel

class ScoreView(
    context: Context,
    attributeSet: AttributeSet?
): ConstraintLayout(context, attributeSet) {

    private lateinit var donutView: DonutProgressView
    private lateinit var donutValueTextView: TextView

    init {
        inflateLayout()
    }

    private fun inflateLayout() {
        inflate(context, R.layout.view_donut, this)
        this.donutView = findViewById(R.id.score_view)
        this.donutValueTextView = findViewById(R.id.score_text)
    }

    fun setScore(scoreModel: ScoreModel) {
        // Set donut text
        val text = context.getString(
            R.string.your_credit_score,
            scoreModel.score,
            scoreModel.maxScore
        )
        val spannable = SpannableString(text)
        val valueStart = text.indexOf("${scoreModel.score}", 0, false)
        val valueEnd = valueStart + "${scoreModel.score}".length
        // Set colour span
        spannable.setSpan(
            ForegroundColorSpan(context.getColor(R.color.gold)),
            valueStart,
            valueEnd,
            0
        )
        // Set size span
        spannable.setSpan(
            RelativeSizeSpan(3f),
            valueStart,
            valueEnd,
            0
        )
        donutValueTextView.text = spannable


        val section1 = DonutSection(
            name = "score",
            color = context.getColor(R.color.gold),
            amount = scoreModel.score.toFloat()
        )
        donutView.cap = scoreModel.maxScore.toFloat()
        donutView.submitData(listOf(section1))
    }
}