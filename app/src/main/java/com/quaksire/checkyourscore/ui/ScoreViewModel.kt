package com.quaksire.checkyourscore.ui

import androidx.lifecycle.ViewModel
import com.quaksire.checkyourscore.use.ScoreModel
import com.quaksire.checkyourscore.use.ScoreUseCase
import com.quaksire.checkyourscore.use.State
import com.quaksire.checkyourscore.util.ScoreIdlingResource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception

class ScoreViewModel(
    private val scoreUseCase: ScoreUseCase
): ViewModel() {

    val scoreFlow: Flow<State<ScoreModel>> = flow {
        ScoreIdlingResource().setIdleState(false)
        emit(State.Loading())
        try {
            val score = scoreUseCase.getUserScore()
            emit(State.Success(score))
        } catch (e: Exception) {
            emit(State.Error())
        }
        ScoreIdlingResource().setIdleState(true)
    }
}