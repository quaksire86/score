package com.quaksire.checkyourscore.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.quaksire.checkyourscore.R
import com.quaksire.checkyourscore.ui.custom.ScoreView
import com.quaksire.checkyourscore.use.State
import com.quaksire.checkyourscore.util.LogManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ScoreFragment: Fragment() {

    private val scoreViewModel: ScoreViewModel by viewModel()

    private lateinit var errorContainer: View
    private lateinit var valueContainer: ScoreView
    private lateinit var loadingContainer: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.score_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.errorContainer = view.findViewById(R.id.error_container)
        this.valueContainer = view.findViewById(R.id.value_container)
        this.loadingContainer = view.findViewById(R.id.loading_container)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                scoreViewModel.scoreFlow.collect { state ->
                    when (state) {
                        is State.Loading -> {
                            LogManager.d(TAG, "Loading")
                            errorContainer.visibility = View.GONE
                            valueContainer.visibility = View.GONE
                            loadingContainer.visibility = View.VISIBLE
                        }
                        is State.Success -> {
                            LogManager.d(TAG, "Success($state)")
                            valueContainer.setScore(state.value)
                            errorContainer.visibility = View.GONE
                            valueContainer.visibility = View.VISIBLE
                            loadingContainer.visibility = View.GONE
                        }
                        is State.Error -> {
                            LogManager.d(TAG, "Error")
                            errorContainer.visibility = View.VISIBLE
                            valueContainer.visibility = View.GONE
                            loadingContainer.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    companion object {
        private const val TAG = "ScoreFragment"
    }
}