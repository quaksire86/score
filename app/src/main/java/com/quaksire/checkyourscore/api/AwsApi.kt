package com.quaksire.checkyourscore.api

import com.quaksire.checkyourscore.api.dto.ScoreDto
import retrofit2.http.GET

interface AwsApi {
    @GET("endpoint.json")
    suspend fun getScore(): ScoreDto
}