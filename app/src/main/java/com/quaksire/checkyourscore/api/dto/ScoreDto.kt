package com.quaksire.checkyourscore.api.dto

data class ScoreDto(
    val creditReportInfo: CreditReportInfoDto?
)