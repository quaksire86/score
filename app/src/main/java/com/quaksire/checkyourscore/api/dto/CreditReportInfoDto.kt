package com.quaksire.checkyourscore.api.dto

import com.google.gson.annotations.SerializedName

data class CreditReportInfoDto(
    @SerializedName("score")
    val score: Int,
    @SerializedName("maxScoreValue")
    val maxScore: Int,
    @SerializedName("minScoreValue")
    val minScore: Int
)