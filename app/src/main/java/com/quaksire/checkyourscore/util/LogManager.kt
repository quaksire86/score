package com.quaksire.checkyourscore.util

import android.util.Log
import com.quaksire.checkyourscore.BuildConfig

object LogManager {
    fun d(tag: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message)
        }
    }

    fun e(tag: String, message: String, e: Exception) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, message, e)
        }
    }
}