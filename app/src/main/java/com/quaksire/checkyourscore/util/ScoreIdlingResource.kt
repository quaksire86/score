package com.quaksire.checkyourscore.util

import androidx.annotation.Nullable
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.IdlingResource.ResourceCallback
import java.util.concurrent.atomic.AtomicBoolean

class ScoreIdlingResource: IdlingResource {

    @Nullable
    @Volatile
    private var mCallback: ResourceCallback? = null

    private val mIsIdleNow = AtomicBoolean(true)

    override fun getName(): String = "ScoreIdlingResource"

    override fun isIdleNow(): Boolean = mIsIdleNow.get()

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.mCallback = callback
    }

    fun setIdleState(isIdleNow: Boolean) {
        mIsIdleNow.set(isIdleNow)
        if (isIdleNow && mCallback != null) {
            mCallback?.onTransitionToIdle()
        }
    }

}