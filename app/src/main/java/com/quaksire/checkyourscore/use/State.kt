package com.quaksire.checkyourscore.use

sealed class State<out T> {

    class Error<T> : State<T>()

    class Loading<T> : State<T>()

    data class Success<T>(val value: T): State<T>()
}