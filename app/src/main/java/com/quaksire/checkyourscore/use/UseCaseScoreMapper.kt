package com.quaksire.checkyourscore.use

import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.domain.Score

class UseCaseScoreMapper: ScoreMapper<Score, ScoreModel> {

    override fun map(t: Score): ScoreModel {
        return ScoreModel(
            score = t.creditReportInfo.score,
            maxScore = t.creditReportInfo.maxScore,
            minScore = t.creditReportInfo.minScore
        )
    }
}