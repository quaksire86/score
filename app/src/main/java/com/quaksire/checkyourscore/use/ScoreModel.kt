package com.quaksire.checkyourscore.use

data class ScoreModel(
    val score: Int,
    val maxScore: Int,
    val minScore: Int
)