package com.quaksire.checkyourscore.use

import com.quaksire.checkyourscore.ScoreMapper
import com.quaksire.checkyourscore.domain.Score
import com.quaksire.checkyourscore.repository.ScoreRepository
import com.quaksire.checkyourscore.util.LogManager
import java.lang.Exception

interface ScoreUseCase {
    suspend fun getUserScore() : ScoreModel
}

class ScoreUseCaseImpl(
    private val scoreRepository: ScoreRepository,
    private val storeUseCaseMapper: ScoreMapper<Score, ScoreModel>
): ScoreUseCase {

    override suspend fun getUserScore() : ScoreModel {
        try {
            val score = scoreRepository.getScore()
            return storeUseCaseMapper.map(score)
        } catch (e: Exception) {
            LogManager.e(TAG, "exception on getUserScore", e)
            throw ScoreUseCaseException()
        }
    }

    companion object {
        private const val TAG: String = "ScoreUseCaseImpl"
    }
}