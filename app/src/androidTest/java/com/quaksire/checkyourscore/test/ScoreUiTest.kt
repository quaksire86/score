package com.quaksire.checkyourscore.test

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.quaksire.checkyourscore.MainActivity
import com.quaksire.checkyourscore.R
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Rule
import org.junit.Test
import com.quaksire.checkyourscore.util.ScoreIdlingResource

class ScoreUiTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private lateinit var server: MockWebServer
    private val mIdlingResource: ScoreIdlingResource = ScoreIdlingResource()

    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register(mIdlingResource)
        server = MockWebServer()
        server.start(8080)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    /**
     * when request is successful, show the correct views and values
     */
    @Test
    fun testRequestSuccessfulWithCorrectViewsAndValues() {
        this.server.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody("""
                    {
                      "accountIDVStatus": "PASS",
                      "creditReportInfo": {
                        "score": 350,
                        "scoreBand": 4,
                        "clientRef": "CS-SED-655426-708782",
                        "status": "MATCH",
                        "maxScoreValue": 700,
                        "minScoreValue": 0,
                        "monthsSinceLastDefaulted": -1,
                        "hasEverDefaulted": false,
                        "monthsSinceLastDelinquent": 1,
                        "hasEverBeenDelinquent": true,
                        "percentageCreditUsed": 44,
                        "percentageCreditUsedDirectionFlag": 1,
                        "changedScore": 0,
                        "currentShortTermDebt": 13758,
                        "currentShortTermNonPromotionalDebt": 13758,
                        "currentShortTermCreditLimit": 30600,
                        "currentShortTermCreditUtilisation": 44,
                        "changeInShortTermDebt": 549,
                        "currentLongTermDebt": 24682,
                        "currentLongTermNonPromotionalDebt": 24682,
                        "currentLongTermCreditLimit": null,
                        "currentLongTermCreditUtilisation": null,
                        "changeInLongTermDebt": -327,
                        "numPositiveScoreFactors": 9,
                        "numNegativeScoreFactors": 0,
                        "equifaxScoreBand": 4,
                        "equifaxScoreBandDescription": "Excellent",
                        "daysUntilNextReport": 9
                      },
                      "dashboardStatus": "PASS",
                      "personaType": "INEXPERIENCED",
                      "coachingSummary": {
                        "activeTodo": false,
                        "activeChat": true,
                        "numberOfTodoItems": 0,
                        "numberOfCompletedTodoItems": 0,
                        "selected": true
                      },
                      "augmentedCreditScore": null
                    }
                """.trimIndent())
        )
        onView(withId(R.id.value_container))
            .check(matches(isDisplayed()))
        onView(withId(R.id.score_view))
            .check(matches(isDisplayed()))
        onView(withId(R.id.score_text))
            .check(matches(withText("Your credit score is \n350 \nout of 700")))
            .check(matches(isDisplayed()))
    }

    /**
     * when json does not contain required values, show error message
     */
    @Test
    fun testMissingRequiredValuesDisplayErrorMessage() {
        this.server.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody("""
                    {
                      "accountIDVStatus": "PASS",
                      "dashboardStatus": "PASS",
                      "personaType": "INEXPERIENCED",
                      "coachingSummary": {
                      },
                      "augmentedCreditScore": null
                    }
                """.trimIndent())
        )
        onView(withId(R.id.error_container))
            .check(matches(isDisplayed()))
    }

    /**
     * when request is an 4XX error, show error message
     */
    @Test
    fun testRequest4xxErrorDisplayErrorMessage() {
        this.server.enqueue(
            MockResponse()
                .setResponseCode(400)
        )
        onView(withId(R.id.error_container))
            .check(matches(isDisplayed()))
    }

    /**
     * when request is an 5XX error, show error message
     */
    @Test
    fun testRequest5xxErrorDisplayErrorMessage() {
        this.server.enqueue(
            MockResponse()
                .setResponseCode(500)
        )
        onView(withId(R.id.error_container))
            .check(matches(isDisplayed()))
    }
}