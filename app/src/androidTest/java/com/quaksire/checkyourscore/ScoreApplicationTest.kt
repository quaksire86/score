package com.quaksire.checkyourscore

import android.app.Application
import com.google.gson.GsonBuilder
import com.quaksire.checkyourscore.api.AwsApi
import com.quaksire.checkyourscore.di.provideRepositoryModule
import com.quaksire.checkyourscore.di.provideUseCaseModule
import com.quaksire.checkyourscore.di.provideViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ScoreApplicationTest: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@ScoreApplicationTest)

            modules(
                provideApiModule(),
                provideRepositoryModule(),
                provideUseCaseModule(),
                provideViewModelModule()
            )
        }
    }

    private fun provideApiModule() = module {
        single<Retrofit> {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .baseUrl("http://127.0.0.1:8080")
                .build()
        }
        single<AwsApi> { get<Retrofit>().create(AwsApi::class.java)}
    }
}